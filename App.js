import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView } from 'react-native';
import { useEffect, useState } from 'react';

export default function App() {
  const [count, setCount] = useState(0);
  const [dado, setDado] = useState(0);

  function rolarDado(){
    setDado(Math.floor(Math.random()* 6) + 1);
  }
  return (
    
    <View style={styles.container}>
      <ScrollView>
      {/* <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text>
      <Text>E o pintinho: piu!</Text> */}

      <Text>Clique para contar: {count} </Text>
      <TouchableOpacity
      style={styles.teste}
      onPress={()=> setCount(count + 1)}
      >
      <Text>Clique Aqui!</Text>
      </TouchableOpacity>
      <Text> </Text>

      <TouchableOpacity
      
      style={styles.teste}
      onPress={() => rolarDado()}
      >
        
        <Text>Jogue o dado!</Text>
        
      </TouchableOpacity>
      <Text>Número Sorteado: {dado}</Text>
      </ScrollView>
      
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8ccd9',
    alignItems: 'center',
    justifyContent: 'center',
  },

  teste: {
    color: "black",
    backgroundColor: "#ea658c",
    width:100,
    borderRadius: 20,
    alignItems: "center",
  },
});
